unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXClass, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    DXTimer1: TDXTimer;
    procedure FormCreate(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  PicItem: TPictureCollectionItem;
  Angle: Cardinal;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
begin
  DXDraw1.Surface.Fill(0);
  PicItem.DrawRotate(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //位置
    PicItem.Width, PicItem.Height,            //大小
    0,                                        //调色板
    0.5, 0.5,                                 //旋转中心:0..1
    Byte(Angle));                             //旋转角度
  Inc(Angle);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXDraw1.Align := alClient;
  DXTimer1.Interval := 0;
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);
end;

end.

