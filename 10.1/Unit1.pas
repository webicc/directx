unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    Button1: TButton;
    Button2: TButton;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}
var
  PicItem: TPictureCollectionItem;

procedure TForm1.Button1Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(0);
  PicItem.StretchDraw(DXDraw1.Surface, DXDraw1.ClientRect, 0);
  DXDraw1.Flip;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  r: TRect;
begin
  r.Left := (DXDraw1.Width - PicItem.Width) div 2;
  r.Top := (DXDraw1.Height - PicItem.Height) div 2;
  r.Right := r.Left + PicItem.Width;
  r.Bottom := r.Top + PicItem.Height;

  DXDraw1.Surface.Fill(0);
  PicItem.StretchDraw(DXDraw1.Surface, r, 0);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);
end;

end.

