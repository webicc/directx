unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXClass, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXTimer1: TDXTimer;
    procedure FormCreate(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
var
  x1, y1, x2, y2: Integer;
  clr: TColor;
begin
  x1 := Random(DXDraw1.Surface.Width);
  y1 := Random(DXDraw1.Surface.Height);
  x2 := Random(DXDraw1.Surface.Width);
  y2 := Random(DXDraw1.Surface.Height);
  clr := Random($FFFFFF);

  DXDraw1.Surface.Lock();
  DXDraw1.Surface.PokeFilledEllipse(x1, y1, x2, y2, clr);
  DXDraw1.Surface.UnLock;
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXDraw1.Align := alClient;
  DXTimer1.Interval := 100;
end;

end.

