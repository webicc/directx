unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls, DXClass, ComCtrls;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    TrackBar1: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  PicItem: TPictureCollectionItem;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\DelphiX.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);

  TrackBar1.Max := 255;
  TrackBar1.Min := 0;
  TrackBar1.Height := 22;
  TrackBar1.ShowSelRange := False;
  TrackBar1.Align := alBottom;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
var
  r: TRect;
begin
  r := Bounds((DXDraw1.Width - PicItem.Width) div 2, (DXDraw1.Height - PicItem.Height)
    div 2, PicItem.Width, PicItem.Height);

  DXDraw1.Surface.Fill(0);
  PicItem.DrawAlpha(DXDraw1.Surface, r, 0, TrackBar1.Position);
  DXDraw1.Flip;
end;

end.

