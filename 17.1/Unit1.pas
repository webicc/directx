unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn5: TButton;
    btn6: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
    procedure btn6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  PicItem: TPictureCollectionItem;
  w, h: Integer;

procedure TForm1.btn1Click(Sender: TObject);
begin
  PicItem.PatternWidth := w;
  PicItem.PatternHeight := h;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  PicItem.PatternWidth := w * 2;
  PicItem.PatternHeight := h;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  PicItem.PatternWidth := w div 2;
  PicItem.PatternHeight := h;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn4Click(Sender: TObject);
begin
  PicItem.PatternWidth := w;
  PicItem.PatternHeight := h * 2;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn5Click(Sender: TObject);
begin
  PicItem.PatternWidth := w;
  PicItem.PatternHeight := h div 2;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn6Click(Sender: TObject);
begin
  PicItem.PatternWidth := w * 2;
  PicItem.PatternHeight := h * 2;
  DXDraw1.Surface.Fill(0);
  PicItem.Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);
  w := PicItem.Width;
  h := PicItem.Height;
end;

end.

