unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws, DirectX, DXClass, Types;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    btn2: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ImgPath1 = '..\pic\delphix.bmp';
  ImgPath2 = '..\pic\back.jpg';

procedure TForm1.btn1Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  DF: TDDBltFX;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.LoadFromFile(ImgPath2);
  DF.dwSize := SizeOf(DF);
  DF.dwDDFX := 0;
  DXDraw1.Surface.Blt( //
    Rect(0, 0, MySurface.Width div 2, MySurface.Height div 2), //
    MySurface.ClientRect, //
    DDBLT_KEYSRC or DDBLT_WAIT, //
    DF, //
    MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.LoadFromFile(ImgPath2);
  DXDraw1.Surface.BltFast(0, 0, //
    Bounds(16, 4, Trunc(MySurface.Width * 0.9), Trunc(MySurface.Height * 0.9)), //
    DDBLTFAST_SRCCOLORKEY or DDBLTFAST_WAIT, //
    MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

end.

