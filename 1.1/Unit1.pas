<<<<<<< HEAD
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls, DXClass;

type
  TForm1 = class(TDXForm)
    DXDraw1: TDXDraw;
    DXTimer1: TDXTimer;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DXDraw1Initialize(Sender: TObject);
    procedure DXDraw1Finalize(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    procedure CalculateTables;
    procedure PlotPoint(XCenter, YCenter, Radius, Angle: Word);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  SineMove: array[0..255] of integer;
  CosineMove: array[0..255] of integer;
  SineTable: array[0..449] of integer;
  CenterX, CenterY: Integer;
  x: Word = 0;
  y: Word = 0;

procedure TForm1.CalculateTables;
var
  wCount: Word;
begin
  for wCount := 0 to 255 do
  begin
    SineMove[wCount] := round(sin(pi * wCount / 128) * 45);
    CosineMove[wCount] := round(cos(pi * wCount / 128) * 60);
  end;
  for wCount := 0 to 449 do
  begin
    SineTable[wCount] := round(sin(pi * wCount / 180) * 128);
  end;
end;

procedure TForm1.PlotPoint(XCenter, YCenter, Radius, Angle: Word);
var
  a, b: Word;
begin
  a := (Radius * SineTable[90 + Angle]);
  asm
        sar     a, 7
  end;
  a := CenterX + XCenter + a;
  b := (Radius * SineTable[Angle]);
  asm
        sar     b, 7
  end;
  b := CenterY + YCenter + b;
  if (a < Width) and (b < Height) then
  begin
    DXImageList1.Items[0].Draw(DXDraw1.Surface, a, b, 0);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXDraw1.Align := alClient;
  CenterX := Width div 2;
  CenterY := Height div 2;
  CalculateTables;
end;

procedure TForm1.DXDraw1Finalize(Sender: TObject);
begin
  DXTimer1.Enabled := False;
end;

procedure TForm1.DXDraw1Initialize(Sender: TObject);
begin
  DXTimer1.Enabled := True;
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
const
  IncAngle = 12;
  XMove = 7;
  YMove = 8;
var
  CountAngle: Word;
  CountLong: Word;
  IncLong: Word;
begin
  if not DXDraw1.CanDraw then
    exit;
  IncLong := 2;
  CountLong := 20;
  DXDraw1.Surface.Fill(0);

  repeat
    CountAngle := 0;
    repeat
      PlotPoint(CosineMove[(x + (200 - CountLong)) mod 255], SineMove[(y + (200 - CountLong)) mod 255], CountLong, CountAngle);
      Inc(CountAngle, IncAngle);
    until CountAngle >= 360;

    inc(CountLong, IncLong);
    if (CountLong mod 3) = 0 then
    begin
      inc(IncLong);
    end;
  until CountLong >= 270;

  x := XMove + x mod 255;
  y := YMove + y mod 255;

  with DXDraw1.Surface.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Color := clWhite;
    Font.Size := 12;
    Textout(0, 0, 'FPS: ' + inttostr(DXTimer1.FrameRate)); { 录制动画是丢了这句 }
    Release;
  end;
  DXDraw1.Flip;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

end.

=======
unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls, DXClass;

type
  TForm1 = class(TDXForm)
    DXDraw1: TDXDraw;
    DXTimer1: TDXTimer;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DXDraw1Initialize(Sender: TObject);
    procedure DXDraw1Finalize(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    procedure CalculateTables;
    procedure PlotPoint(XCenter, YCenter, Radius, Angle: Word);
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  SineMove: array[0..255] of integer;
  CosineMove: array[0..255] of integer;
  SineTable: array[0..449] of integer;
  CenterX, CenterY: Integer;
  x: Word = 0;
  y: Word = 0;

procedure TForm1.CalculateTables;
var
  wCount: Word;
begin
  for wCount := 0 to 255 do
  begin
    SineMove[wCount] := round(sin(pi * wCount / 128) * 45);
    CosineMove[wCount] := round(cos(pi * wCount / 128) * 60);
  end;
  for wCount := 0 to 449 do
  begin
    SineTable[wCount] := round(sin(pi * wCount / 180) * 128);
  end;
end;

procedure TForm1.PlotPoint(XCenter, YCenter, Radius, Angle: Word);
var
  a, b: Word;
begin
  a := (Radius * SineTable[90 + Angle]);
  asm
        sar     a, 7
  end;
  a := CenterX + XCenter + a;
  b := (Radius * SineTable[Angle]);
  asm
        sar     b, 7
  end;
  b := CenterY + YCenter + b;
  if (a < Width) and (b < Height) then
  begin
    DXImageList1.Items[0].Draw(DXDraw1.Surface, a, b, 0);
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXDraw1.Align := alClient;
  CenterX := Width div 2;
  CenterY := Height div 2;
  CalculateTables;
end;

procedure TForm1.DXDraw1Finalize(Sender: TObject);
begin
  DXTimer1.Enabled := False;
end;

procedure TForm1.DXDraw1Initialize(Sender: TObject);
begin
  DXTimer1.Enabled := True;
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
const
  IncAngle = 12;
  XMove = 7;
  YMove = 8;
var
  CountAngle: Word;
  CountLong: Word;
  IncLong: Word;
begin
  if not DXDraw1.CanDraw then
    exit;
  IncLong := 2;
  CountLong := 20;
  DXDraw1.Surface.Fill(0);

  repeat
    CountAngle := 0;
    repeat
      PlotPoint(CosineMove[(x + (200 - CountLong)) mod 255], SineMove[(y + (200 - CountLong)) mod 255], CountLong, CountAngle);
      Inc(CountAngle, IncAngle);
    until CountAngle >= 360;

    inc(CountLong, IncLong);
    if (CountLong mod 3) = 0 then
    begin
      inc(IncLong);
    end;
  until CountLong >= 270;

  x := XMove + x mod 255;
  y := YMove + y mod 255;

  with DXDraw1.Surface.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Color := clWhite;
    Font.Size := 12;
    Textout(0, 0, 'FPS: ' + inttostr(DXTimer1.FrameRate)); { 录制动画是丢了这句 }
    Release;
  end;
  DXDraw1.Flip;
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then
    Close;
end;

end.

>>>>>>> c5365ffdf613f3814fd3114a21a7d031c6b6588a
