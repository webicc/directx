object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 178
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DXDraw1: TDXDraw
    Left = 8
    Top = 8
    Width = 478
    Height = 121
    AutoInitialize = True
    AutoSize = True
    Color = clBlack
    Display.FixedBitCount = False
    Display.FixedRatio = True
    Display.FixedSize = True
    Options = [doAllowReboot, doWaitVBlank, doCenter, do3D, doDirectX7Mode, doHardware, doSelectDriver]
    SurfaceHeight = 121
    SurfaceWidth = 478
    TabOrder = 0
    Traces = <>
  end
  object Button1: TButton
    Left = 8
    Top = 145
    Width = 75
    Height = 25
    Caption = 'File'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 89
    Top = 145
    Width = 75
    Height = 25
    Caption = 'Memory'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 170
    Top = 145
    Width = 75
    Height = 25
    Caption = 'Graphic'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 251
    Top = 145
    Width = 75
    Height = 25
    Caption = 'BitMap'
    TabOrder = 4
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 332
    Top = 145
    Width = 75
    Height = 25
    Caption = 'DIB'
    TabOrder = 5
    OnClick = Button5Click
  end
  object Button6: TButton
    Left = 413
    Top = 145
    Width = 75
    Height = 25
    Caption = 'DIBRect'
    TabOrder = 6
    OnClick = Button6Click
  end
end
