unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws, DIB;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ImgPath1 = '..\pic\delphix.bmp';

procedure TForm1.Button1Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  MySurface.LoadFromFile(ImgPath1);

  DXDraw1.Surface.Fill($EEEEEE);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  stream: TMemoryStream;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  stream := TMemoryStream.Create;
  stream.LoadFromFile(ImgPath1);

  MySurface.LoadFromStream(stream);

  DXDraw1.Surface.Fill($CCCCCC);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;

  FreeAndNil(stream);
  FreeAndNil(MySurface);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  bit: TGraphic;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  bit := TBitmap.Create;
  bit.LoadFromFile(ImgPath1);

  MySurface.LoadFromGraphic(bit);

  DXDraw1.Surface.Fill($999999);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;

  FreeAndNil(bit);
  FreeAndNil(MySurface);
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  bit: TBitmap;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  bit := TBitmap.Create;
  bit.LoadFromFile(ImgPath1);

  MySurface.LoadFromGraphicRect(bit, bit.Width div 2, bit.Height div 2, Rect(0, 0, bit.Width, bit.Height));

  DXDraw1.Surface.Fill($666666);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;

  FreeAndNil(bit);
  FreeAndNil(MySurface);
end;

procedure TForm1.Button5Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  dib: TDIB;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  dib := TDIB.Create;
  dib.LoadFromFile(ImgPath1);

  MySurface.LoadFromDIB(dib);

  DXDraw1.Surface.Fill($333333);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;

  FreeAndNil(dib);
  FreeAndNil(MySurface);
end;

procedure TForm1.Button6Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  dib: TDIB;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);

  dib := TDIB.Create;
  dib.LoadFromFile(ImgPath1);

  MySurface.LoadFromDIBRect(dib, //
    DXDraw1.Width, DXDraw1.Height, //
    Rect(0, 0, dib.Width, dib.Height));

  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;

  FreeAndNil(dib);
  FreeAndNil(MySurface);
end;

end.

