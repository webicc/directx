unit Unit1;   //这里

interface    //这里

uses         //这里
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXClass, DXDraws, StdCtrls;

type
  TForm1 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    DXTimer1: TDXTimer;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var      //这里
  Form1: TForm1;

implementation//这里

{$R *.dfm}

var
  arr: array of record
    X, Y, a, b: Integer;
  end;
  PicItem: TPictureCollectionItem;

procedure TForm1.btn1Click(Sender: TObject);
begin
  if Length(arr) > 1 then
    SetLength(arr, Length(arr) - 1);
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  SetLength(arr, Length(arr) + 1);
  arr[High(arr)].X := Random(DXDraw1.Width - PicItem.Width);
  arr[High(arr)].Y := Random(DXDraw1.Height - PicItem.Height);
  arr[High(arr)].a := 1;
  arr[High(arr)].b := 1;
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
var
  i: Integer;
begin
  DXDraw1.Surface.Fill(0);
  for i := 0 to Length(arr) - 1 do
  begin
    if arr[i].x > DXDraw1.Width - PicItem.Width then
      arr[i].a := -1;
    if arr[i].Y > DXDraw1.Height - PicItem.Height then
      arr[i].b := -1;
    if arr[i].x = 0 then
      arr[i].a := 1;
    if arr[i].Y = 0 then
      arr[i].b := 1;
    Inc(arr[i].x, arr[i].a);
    Inc(arr[i].Y, arr[i].b);
    PicItem.Draw(DXDraw1.Surface, arr[i].x, arr[i].y, 0);
  end;
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXDraw1.Align := alClient;
  DXImageList1.DXDraw := DXDraw1;

  DXImageList1.Items.Add;
  PicItem := DXImageList1.Items[DXImageList1.Items.Count - 1];
  PicItem.Picture.LoadFromFile(ImgPath1);

  SetLength(arr, 1);
  arr[0].X := Random(DXDraw1.Width - PicItem.Width);
  arr[0].Y := Random(DXDraw1.Height - PicItem.Height);
  arr[0].a := 1;
  arr[0].b := 1;

  btn1.Caption := '-';
  btn2.Caption := '+';

  DXTimer1.Interval := 10;
end;

end.

