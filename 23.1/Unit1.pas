unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ImgPath1 = '..\pic\delphix.bmp';

procedure TForm1.btn1Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.LoadFromFile(ImgPath1);
  DXDraw1.Flip;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Lock();
  DXDraw1.Surface.Blur;
  DXDraw1.Surface.UnLock;
  DXDraw1.Flip;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  with DXDraw1.Surface do
  begin
    Fill(0);
    LoadFromFile(ImgPath1);
    Lock();
    Blur;
    Blur;
    UnLock;
  end;
  DXDraw1.Flip;
end;

end.

