unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    DXImageList1: TDXImageList;
    btn2: TButton;
    procedure btn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
begin
  DXImageList1.Items.LoadFromFile('delphix.dxg');
  DXImageList1.Items[0].Draw(DXDraw1.Surface, 10, 10, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  DXImageList1.Items.LoadFromFile('delphix.jpg');
  DXImageList1.Items[1].Draw(DXDraw1.Surface, 10, 10, 0);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXImageList1.DXDraw := DXDraw1;
end;

end.

