unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls;

type
  TForm1 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  PicItem: TPictureCollectionItem;

procedure TForm1.btn1Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  PicItem.Restore; {Surface.Draw的矩形范围好像能直接作用到列表中的图片,需要恢复一下}
  MySurface.SetSize(PicItem.Width, PicItem.Height);
  MySurface.Fill(0);
  PicItem.Draw(MySurface, 0, 0, 0);

  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw((DXDraw1.Width - MySurface.Width) div 2, //
    (DXDraw1.Height - MySurface.Height) div 2, //
    MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  m, n: Integer;
begin
  m := 40;
  n := 14;
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  PicItem.Restore;
  MySurface.SetSize(PicItem.Width, PicItem.Height);
  MySurface.Fill(0);
  PicItem.Draw(MySurface, 0, 0, 0);

  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw((DXDraw1.Width - MySurface.Width) div 2 + m, //
    (DXDraw1.Height - MySurface.Height) div 2 + n, //
    Bounds(m, n, MySurface.Width - m * 2, MySurface.Height - n * 2), //
    MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.add);
  PicItem.Picture.LoadFromFile(ImgPath1);
end;

end.

