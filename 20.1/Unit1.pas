unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    btn5: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
    procedure btn5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ImgPath1 = '..\pic\back.jpg';

procedure TForm1.btn1Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);
  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  r: Trect;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);

  r := Bounds(10, 10, DXDraw1.Width - 30, DXDraw1.Height - 30);
  DXDraw1.Surface.FillRect(r, $FF0000);

  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn3Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  r: Trect;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);

  r := Bounds(10, 10, DXDraw1.Width - 30, DXDraw1.Height - 30);
  DXDraw1.Surface.FillRectAlpha(r, $0000FF, 128);

  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn4Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  r: Trect;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);

  r := Bounds(10, 10, DXDraw1.Width - 30, DXDraw1.Height - 30);
  DXDraw1.Surface.FillRectAdd(r, $0000FF, 255);

  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.btn5Click(Sender: TObject);
var
  MySurface: TDirectDrawSurface;
  r: Trect;
begin
  MySurface := TDirectDrawSurface.Create(DXDraw1.DDraw);
  MySurface.LoadFromFile(ImgPath1);
  DXDraw1.Surface.Fill(0);
  DXDraw1.Surface.Draw(0, 0, MySurface);

  r := Bounds(10, 10, DXDraw1.Width - 30, DXDraw1.Height - 30);
  DXDraw1.Surface.FillRectSub(r, $0000FF, 255);

  DXDraw1.Flip;
  FreeAndNil(MySurface);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  btn1.Caption := 'ԭʼͼƬ';
  btn2.Caption := 'FillRect';
  btn3.Caption := 'FillRectAlpha';
  btn4.Caption := 'FillRectAdd';
  btn5.Caption := 'FillRectSub';
end;

end.

