object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 213
  ClientWidth = 349
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object DXDraw1: TDXDraw
    Left = 8
    Top = 8
    Width = 225
    Height = 193
    AutoInitialize = True
    AutoSize = True
    Color = clBlack
    Display.FixedBitCount = False
    Display.FixedRatio = True
    Display.FixedSize = True
    Options = [doAllowReboot, doWaitVBlank, doCenter, do3D, doDirectX7Mode, doHardware, doSelectDriver]
    SurfaceHeight = 193
    SurfaceWidth = 225
    TabOrder = 0
    Traces = <>
  end
  object btn1: TButton
    Left = 239
    Top = 8
    Width = 102
    Height = 25
    Caption = 'btn1'
    TabOrder = 1
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 239
    Top = 50
    Width = 102
    Height = 25
    Caption = 'btn2'
    TabOrder = 2
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 239
    Top = 92
    Width = 102
    Height = 25
    Caption = 'btn3'
    TabOrder = 3
    OnClick = btn3Click
  end
  object btn4: TButton
    Left = 239
    Top = 134
    Width = 102
    Height = 25
    Caption = 'btn4'
    TabOrder = 4
    OnClick = btn4Click
  end
  object btn5: TButton
    Left = 239
    Top = 176
    Width = 102
    Height = 25
    Caption = 'btn5'
    TabOrder = 5
    OnClick = btn5Click
  end
end
