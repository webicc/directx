unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    Button1: TButton;
    Button2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  pic1, pic2: TPictureCollectionItem;

procedure TForm1.Button1Click(Sender: TObject);
var
  r: TRect;
begin
  r := Bounds((DXDraw1.Width - pic2.Width) div 2, (DXDraw1.Height - pic2.Height)
    div 2, pic2.Width, pic2.Height);
  DXDraw1.Surface.Fill(0);
  pic1.StretchDraw(DXDraw1.Surface, DXDraw1.ClientRect, 0);
  pic2.DrawAdd(DXDraw1.Surface, r, 0);
  DXDraw1.Flip;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  r: TRect;
begin
  r := Bounds((DXDraw1.Width - pic2.Width) div 2, (DXDraw1.Height - pic2.Height)
    div 2, pic2.Width, pic2.Height);
  DXDraw1.Surface.Fill(0);
  pic1.StretchDraw(DXDraw1.Surface, DXDraw1.ClientRect, 0);
  pic2.DrawSub(DXDraw1.Surface, r, 0);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\back.jpg';
  ImgPath2 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  pic1 := TPictureCollectionItem(DXImageList1.Items.Add);
  pic1.Picture.LoadFromFile(ImgPath1);
  pic2 := TPictureCollectionItem(DXImageList1.Items.Add);
  pic2.Picture.LoadFromFile(ImgPath2);
end;

end.

