unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws, DXClass;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    btn2: TButton;
    DXTimer1: TDXTimer;
    procedure RedShow;
    procedure GreenShow;
    procedure DXDraw1Initialize(Sender: TObject);
    procedure DXDraw1Finalize(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.DXDraw1Finalize(Sender: TObject);
begin
  DXTimer1.Enabled := False;
end;

procedure TForm1.DXDraw1Initialize(Sender: TObject);
begin
  DXTimer1.Enabled := True;
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
begin
  RedShow;
  Sleep(1000);
  GreenShow;
  Sleep(1000);
  Application.ProcessMessages;
end;

procedure TForm1.GreenShow;
var
  str: string;
begin
  str := FormatDateTime('hh:mm:ss:zz', Time);
  DXDraw1.Surface.Fill(0);
  with DXDraw1.Surface.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Color := clLime;
    TextOut(10, 10, str);
    Release;
  end;
  DXDraw1.Flip;
end;

procedure TForm1.RedShow;
var
  str: string;
begin
  str := FormatDateTime('hh:mm:ss:zz', Time); {获取当前时间,精确到毫秒}
  DXDraw1.Surface.Fill($FF0000); {填充为红色和HTML格式一样}
  with DXDraw1.Surface.Canvas do
  begin
    Brush.Style := bsClear;
    Font.Color := clYellow;
    Font.Size := 16;
    TextOut(10, 10, str);
    Release;
  end;
  DXDraw1.Flip;
end;

end.

