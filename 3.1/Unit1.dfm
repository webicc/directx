object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 168
  ClientWidth = 293
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DXDraw1: TDXDraw
    Left = 24
    Top = 8
    Width = 249
    Height = 121
    AutoInitialize = True
    AutoSize = True
    Color = clBlack
    Display.FixedBitCount = False
    Display.FixedRatio = True
    Display.FixedSize = True
    Options = [doAllowReboot, doWaitVBlank, doCenter, do3D, doDirectX7Mode, doHardware, doSelectDriver]
    SurfaceHeight = 121
    SurfaceWidth = 249
    OnFinalize = DXDraw1Finalize
    OnInitialize = DXDraw1Initialize
    TabOrder = 0
    Traces = <>
  end
  object btn1: TButton
    Left = 48
    Top = 135
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 1
  end
  object btn2: TButton
    Left = 158
    Top = 135
    Width = 75
    Height = 25
    Caption = 'btn2'
    TabOrder = 2
  end
  object DXTimer1: TDXTimer
    ActiveOnly = True
    Enabled = True
    Interval = 1000
    OnTimer = DXTimer1Timer
    Left = 248
    Top = 136
  end
end
