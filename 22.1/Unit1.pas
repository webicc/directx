unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    TrackBar1: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXDraw1.Align := alClient;
  with TrackBar1 do
  begin
    Align := alBottom;
    Height := 22;
    ShowSelRange := False;
    max := 255;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  with DXDraw1.Surface do
  begin
    Fill(0);
    Lock();
    Noise(ClientRect, TrackBar1.Position);
    UnLock;
  end;
  DXDraw1.Flip;
end;

end.

