unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    btn1: TButton;
    btn2: TButton;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(0);
  DXImageList1.Items.Find('img1').Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(0);
  DXImageList1.Items.Find('img2').Draw(DXDraw1.Surface, 0, 0, 0);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
  ImgPath2 = '..\pic\delphix.jpg';
begin
  DXImageList1.DXDraw := DXDraw1;

  DXImageList1.Items.Add;
  DXImageList1.Items[DXImageList1.Items.Count - 1].Picture.LoadFromFile(ImgPath1);
  DXImageList1.Items[DXImageList1.Items.Count - 1].Name := 'img1';

  DXImageList1.Items.Add;
  DXImageList1.Items[DXImageList1.Items.Count - 1].Picture.LoadFromFile(ImgPath2);
  DXImageList1.Items[DXImageList1.Items.Count - 1].Name := 'img2';
end;

end.

