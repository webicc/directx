unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    DXImageList1: TDXImageList;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  Angle = 32; {45度角}
  clr = $777777;

var
  PicItem: TPictureCollectionItem;

procedure TForm1.Button2Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateSub(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle);
  DXDraw1.Flip;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateAlpha(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle, //
    128);
  DXDraw1.Flip;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateAddCol(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle, //
    clYellow);
  DXDraw1.Flip;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateSubCol(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle, //
    clYellow);
  DXDraw1.Flip;
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateAlphaCol(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle, //
    clYellow);
  DXDraw1.Flip;
end;

procedure TForm1.Button7Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotate(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle);
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);

  Button1.Caption := 'DrawRotateAdd';
  Button2.Caption := 'DrawRotateSub';
  Button3.Caption := 'DrawRotateAlpha';
  Button4.Caption := 'DrawRotateAddCol';
  Button5.Caption := 'DrawRotateSubCol';
  Button6.Caption := 'DrawRotateAlphaCol';
  Button7.Caption := '原始效果';

end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  DXDraw1.Surface.Fill(clr);
  PicItem.DrawRotateAdd(DXDraw1.Surface, //
    DXDraw1.Width div 2, DXDraw1.Height div 2, //
    PicItem.Width, PicItem.height, //
    0, //
    0.5, 0.5, //
    Angle);
  DXDraw1.Flip;
end;

end.

