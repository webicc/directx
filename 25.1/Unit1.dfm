object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 187
  ClientWidth = 449
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object btn1: TButton
    Left = 8
    Top = 152
    Width = 75
    Height = 25
    Caption = 'btn1'
    TabOrder = 0
    OnClick = btn1Click
  end
  object btn2: TButton
    Left = 128
    Top = 152
    Width = 75
    Height = 25
    Caption = 'btn2'
    TabOrder = 1
    OnClick = btn2Click
  end
  object btn3: TButton
    Left = 248
    Top = 152
    Width = 75
    Height = 25
    Caption = 'btn3'
    TabOrder = 2
    OnClick = btn3Click
  end
  object btn4: TButton
    Left = 364
    Top = 152
    Width = 75
    Height = 25
    Caption = 'btn4'
    TabOrder = 3
    OnClick = btn4Click
  end
  object DXDraw1: TDXDraw
    Left = 8
    Top = 8
    Width = 431
    Height = 121
    AutoInitialize = True
    AutoSize = True
    Color = clBlack
    Display.FixedBitCount = False
    Display.FixedRatio = True
    Display.FixedSize = True
    Options = [doAllowReboot, doWaitVBlank, doCenter, do3D, doDirectX7Mode, doHardware, doSelectDriver]
    SurfaceHeight = 121
    SurfaceWidth = 431
    TabOrder = 4
    Traces = <>
  end
  object DXDIB1: TDXDIB
    Left = 344
    Top = 64
  end
end
