unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DXDraws, StdCtrls, DIB;

type
  TForm1 = class(TForm)
    btn1: TButton;
    btn2: TButton;
    btn3: TButton;
    btn4: TButton;
    DXDraw1: TDXDraw;
    DXDIB1: TDXDIB;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure btn4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  ImgPath1 = '..\pic\delphix.bmp';

procedure TForm1.btn1Click(Sender: TObject);
begin
  DXDraw1.Surface.LoadFromFile(ImgPath1);
  DXDraw1.Flip;
end;

procedure TForm1.btn2Click(Sender: TObject);
begin
  DXDIB1.DIB.LoadFromFile(ImgPath1);
  DXDIB1.DIB.Blur(32, 1);
  DXDraw1.Surface.LoadFromDIB(DXDIB1.DIB);
  DXDraw1.Flip;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  DXDIB1.DIB.LoadFromFile(ImgPath1);
  DXDIB1.DIB.Blur(32, 6);
  DXDraw1.Surface.LoadFromDIB(DXDIB1.DIB);
  DXDraw1.Flip;
end;

procedure TForm1.btn4Click(Sender: TObject);
begin
  DXDIB1.DIB.LoadFromFile(ImgPath1);
  DXDIB1.DIB.Blur(4, 2);
  DXDraw1.Surface.LoadFromDIB(DXDIB1.DIB);
  DXDraw1.Flip;
end;

end.

