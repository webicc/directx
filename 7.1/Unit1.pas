unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    btn1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
begin
  with DXImageList1 do
  begin
    Items[0].Draw(DXDraw.Surface, 0, 0, 0);
    Items[1].Draw(DXDraw1.Surface, 0, Items[0].Height, 0);
    DXDraw.Flip;
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = 'c:\temp\delphix.bmp';
  ImgPath2 = 'c:\temp\delphix.jpg';
var
  Picitem: TPictureCollectionItem;
begin
  DXImageList1.DXDraw := DXDraw1;

  DXImageList1.Items.Add;
  Picitem := DXImageList1.Items[DXImageList1.Items.Count - 1];
  Picitem.Picture.LoadFromFile(ImgPath1);

  DXImageList1.Items.Add;
  Picitem := DXImageList1.Items[DXImageList1.Items.Count - 1];
  Picitem.Picture.LoadFromFile(ImgPath2);

end;

end.

