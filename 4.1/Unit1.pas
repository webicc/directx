unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DXClass, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXTimer1: TDXTimer;
    TrackBar1: TTrackBar;
    procedure FormCreate(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
    procedure TrackBar1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
var
  x1, y1, x2, y2: Integer;
begin
  x1 := Random(DXDraw1.Width);
  y1 := Random(DXDraw1.Height);
  x2 := Random(DXDraw1.Width);
  y2 := Random(DXDraw1.Height);

  DXDraw1.Surface.Fill(Random(255));
  with DXDraw1.Surface.Canvas do
  begin
    Brush.Color := Random($FFFFFF); {�����ɫ}
    Ellipse(x1, y1, x2, y2);
    Release;
  end;
  DXDraw1.Flip;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  DXDraw1.Align := alClient;
  with TrackBar1 do
  begin
    Align := alBottom;
    Height := 23;
    ShowSelRange := False;
    min := 0;
    max := 500;
    Position := Max div 2;
  end;
  DXTimer1.Interval := TrackBar1.Position;
  Randomize;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  DXTimer1.Interval := TrackBar1.Position;
end;

end.

