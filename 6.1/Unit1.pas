unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXImageList1: TDXImageList;
    btn1: TButton;
    btn2: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.btn1Click(Sender: TObject);
begin
  Self.Refresh;
  Self.Canvas.Draw(0, 0, DXImageList1.Items[0].Picture.Graphic);
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  picItem: TPictureCollectionItem;
begin
  Self.Refresh;
  picItem := DXImageList1.Items[0];
  self.Canvas.StretchDraw(ClientRect, picItem.Picture.Graphic);
end;

procedure TForm1.FormCreate(Sender: TObject);
var
  pic: TPicture;
begin
  pic := TPicture.Create;
  pic.LoadFromFile('test.bmp');
  DXImageList1.Items.Add;
  DXImageList1.Items[0].Picture.Assign(pic);
  pic.Free;
end;

end.

