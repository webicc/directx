unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }    //为什么啊为什么不见了
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

var
  PicItem: TPictureCollectionItem;

procedure TForm1.CheckBox1Click(Sender: TObject);
const
  arr: array[Boolean] of string = ('不透明', '透明');
begin
  DXDraw1.Surface.Fill($FF0000);
  PicItem.Restore;
  PicItem.Transparent := CheckBox1.Checked;
  PicItem.Draw(DXDraw1.Surface, 10, 10, 0);
  DXDraw1.Flip;
  CheckBox1.Caption := arr[CheckBox1.Checked];
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\transparent.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.add);
  PicItem.Picture.LoadFromFile(ImgPath1);

  PicItem.TransparentColor := clWhite;
end;

end.

