unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DXClass, DXDraws;

type
  TForm1 = class(TForm)
    DXDraw1: TDXDraw;
    DXImageList1: TDXImageList;
    DXTimer1: TDXTimer;
    Edit1: TEdit;
    CheckBox1: TCheckBox;
    procedure Edit1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure DXTimer1Timer(Sender: TObject; LagCount: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

const
  arr: array[0..31] of Smallint = (//
    -8, -7, -6, -5, -4, -3, -2, -1, //
    0, 1, 2, 3, 4, 5, 6, 7, //
    8, 7, 6, 5, 4, 3, 2, 1,  //
    0, -1, -2, -3, -4, -5, -6, -7);

var
  PicItem: TPictureCollectionItem;
  amp, len: Integer;

procedure TForm1.CheckBox1Click(Sender: TObject);
const
  bs: array[Boolean] of string = ('DrawWaveY', 'DrawWaveX');
begin
  CheckBox1.Caption := bs[CheckBox1.Checked];
end;

procedure TForm1.DXTimer1Timer(Sender: TObject; LagCount: Integer);
begin
  DXDraw1.Surface.Fill(0);

  if CheckBox1.Checked then
    PicItem.DrawWaveX(DXDraw1.Surface, //
      (DXDraw1.width - PicItem.Width) div 2, //
      (DXDraw1.Height - PicItem.Height) div 2, //
      PicItem.Width, //
      PicItem.Height, //
      0, //
      arr[amp and 31], // 0..31之间连续变化的数字
      len, //
      0)  //
  else
    PicItem.DrawWaveY(DXDraw1.Surface, //
      (DXDraw1.width - PicItem.Width) div 2, //
      (DXDraw1.Height - PicItem.Height) div 2, //
      PicItem.Width, //
      PicItem.Height, //
      0, //
      arr[amp and 31], //
      len, //
      0);
  DXDraw1.Flip;
  Inc(amp);
end;

procedure TForm1.Edit1Change(Sender: TObject);
begin
  len := StrToIntDef(Edit1.Text, 0);
end;

procedure TForm1.FormCreate(Sender: TObject);
const
  ImgPath1 = '..\pic\delphix.bmp';
begin
  DXImageList1.DXDraw := DXDraw1;
  PicItem := TPictureCollectionItem(DXImageList1.Items.Add);
  PicItem.Picture.LoadFromFile(ImgPath1);

  DXDraw1.Align := alClient;
  DXTimer1.Interval := 30;

  Edit1.Text := '60';
  Edit1Change(nil);

  CheckBox1.Checked := True;
  CheckBox1.Caption := 'DrawWaveX';
end;

end.

